from django.contrib import admin
from django.db import models
from models import Photo, Config
from djangosnippets.snippets.AdminImageWidget import AdminImageWidget


class PhotoAdmin(admin.ModelAdmin):
    formfield_overrides = {
        models.ImageField: {'widget': AdminImageWidget},
    }

admin.site.register(Photo, PhotoAdmin)
admin.site.register(Config)
