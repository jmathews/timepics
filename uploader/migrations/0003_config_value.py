# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0002_auto_20141227_0609'),
    ]

    operations = [
        migrations.AddField(
            model_name='config',
            name='value',
            field=models.CharField(max_length=1024, null=True),
            preserve_default=True,
        ),
    ]
