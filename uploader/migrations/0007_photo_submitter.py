# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0006_auto_20141227_2051'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='submitter',
            field=models.EmailField(default='photographer@example.org', max_length=75),
            preserve_default=False,
        ),
    ]
