# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0015_auto_20141230_0643'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='submitter_email',
            field=models.EmailField(max_length=254, null=True),
            preserve_default=True,
        ),
    ]
