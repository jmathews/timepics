# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0013_photo_is_copied_to_approved_folder'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='submitter_twitter',
            field=models.TextField(max_length=15, null=True),
            preserve_default=True,
        ),
    ]
