# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0005_photo_submission_timestamp'),
    ]

    operations = [
        migrations.AlterField(
            model_name='photo',
            name='submission_timestamp',
            field=models.DateTimeField(auto_now_add=True),
            preserve_default=True,
        ),
    ]
