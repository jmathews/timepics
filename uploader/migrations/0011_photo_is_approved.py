# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('uploader', '0010_photo_photo_time'),
    ]

    operations = [
        migrations.AddField(
            model_name='photo',
            name='is_approved',
            field=models.BooleanField(default=False),
            preserve_default=True,
        ),
    ]
