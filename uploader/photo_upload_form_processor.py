from django.contrib import messages
from PIL import Image
from models import Config
from forms import PhotoUploadForm
from ipware.ip import get_ip


class BadRequest(Exception):
    pass


class BadForm(Exception):
    pass


class PhotoUploadFormProcessor:
    def __init__(self, request, photo_upload_form):
        if request is None:
            raise BadRequest('request is None')

        if photo_upload_form is None:
            raise BadForm('photo_upload_form is None')

        if not isinstance(photo_upload_form, PhotoUploadForm):
            raise BadForm('photo_upload_form is not a PhotoUploadForm')

        self._request = request
        self._photo_upload_form = photo_upload_form

    def process(self):
        return self._process_photo_upload_form()

    def _process_photo_upload_form(self):
        if self._photo_upload_form.is_valid():
            return self._save_photo_from_form()
        else:
            for error, message in self._photo_upload_form.errors.iteritems():
                messages.error(self._request, '{filename} failed validation with the following message: {validation_error}'
                               .format(filename=self._photo_upload_form.get_original_filename(),
                                       validation_error=message[0]))
            return False

    def _save_photo_from_form(self):
        self._photo_file = self._photo_upload_form.save(commit=False)

        if not (self._is_file_present() and
                self._is_valid_image_file() and
                self._is_resolution_valid() and
                self._is_filesize_valid()):
            return False

        self._photo_file.submitter_IP = get_ip(self._request)
        self._photo_file.save()
        messages.success(self._request, '{filename} has been uploaded.'
                         .format(filename=self._photo_upload_form.get_original_filename()
        ))
        return True

    def _is_file_present(self):
        if not self._photo_file.photo_file:
            messages.debug(self._request, 'No file present in this form')
            return False
        return True

    def _is_valid_image_file(self):
        if not self._photo_file.photo_file.name:
            messages.error(self._request, '{filename} is not a valid image file.'
                           .format(filename=self._photo_file.photo_file.name))
            return False
        return True

    def _is_resolution_valid(self):
        min_width = Config.get_config_int('min_width', 1200)
        min_height = Config.get_config_int('min_height', 800)
        image = Image.open(self._photo_file.photo_file)
        width, height = image.size
        width_is_valid = width >= min_width
        height_is_valid = height >= min_height
        if not (width_is_valid and height_is_valid):
            messages.error(self._request,
                           '{filename} does not meet the minimum dimensions required for this project. '
                           'Your image is {width}x{height}. '
                           'It should be at least {min_width}x{min_height}'
                           .format(filename=self._photo_file.photo_file.name,
                                   width=width,
                                   height=height,
                                   min_width=min_width,
                                   min_height=min_height))
            return False
        return True

    def _is_filesize_valid(self):
        max_file_size_MB = Config.get_config_int('max_file_size_MB', 10)
        max_file_size_bytes = max_file_size_MB * 1024 * 1024
        if not self._photo_file.photo_file.size <= max_file_size_bytes:
            messages.error(self._request,
                           '{filename} is bigger than the maximum file size for this project. '
                           'Your file can be at most {max_file_size_MB}MB. Your file is {filesize_MB}MB.'
                           .format(filename=self._photo_file.photo_file.name,
                                   max_file_size_MB=max_file_size_MB,
                                   filesize_MB=round(self._photo_file.photo_file.size / 1024.0 / 1024.0, ndigits=2)))
            return False
        return True
