from django.db import models
from django.db.models.signals import pre_save
from django.dispatch import receiver
from os import path, makedirs
from timepics.settings import MEDIA_ROOT
import shutil
import twitter
from django.template.loader import render_to_string
from django.contrib.sites.models import Site


class Photo(models.Model):

    photo_file = models.ImageField(upload_to='uploads', null=True)
    photo_time = models.TimeField(blank=False, null=False)
    submission_timestamp = models.DateTimeField(auto_now_add=True, null=False, blank=False, editable=True)
    submitter_email = models.EmailField(max_length=254, null=True, blank=True)
    submitter_twitter = models.CharField(max_length=15, null=True, blank=True)
    submitter_IP = models.GenericIPAddressField()
    is_approved = models.BooleanField(default=False)
    is_rejected = models.BooleanField(default=False)
    is_copied_to_approved_folder = models.BooleanField(default=False)
    is_posted_to_twitter = models.BooleanField(default=False)

    def __unicode__(self):
        if self.is_approved:
            approval_str = 'Approved'
        else:
            approval_str = 'Not Approved'
        return '{filename} - ({is_approved})'.format(filename=str(self.photo_file.name), is_approved=approval_str)

    def _get_hour_and_minute(self):
        current_hour = str(self.photo_time.hour).zfill(2)
        current_minute = str(self.photo_time.minute).zfill(2)
        return current_hour, current_minute

    def copy_to_approved_folder(self):
        if self.is_copied_to_approved_folder:
            return self.is_copied_to_approved_folder
        else:
            if self.photo_file and self.photo_file.path:
                current_hour, current_minute = self._get_hour_and_minute()
                image_folder = path.join(MEDIA_ROOT, 'approved', current_hour, current_minute)
                if not path.isdir(image_folder):
                    makedirs(image_folder)
                shutil.copy(self.photo_file.path, image_folder)
            else:
                return False
            return True

    def compose_tweet(self):
        context = {'photo': self,
                   'site':  Site.objects.get_current()}
        if self.submitter_twitter and len(self.submitter_twitter) > 1:
            context['submitter_attribution'] = self.submitter_twitter
        elif self.submitter_email and len(self.submitter_email) > 1:
            index_of_at = self.submitter_email.find('@')
            if index_of_at > 1:
                context['submitter_attribution'] = self.submitter_email[:index_of_at]
        return render_to_string('uploader/photo_upload.tweet', context)

    def tweet_photo(self):
        if self.is_posted_to_twitter:
            return self.is_posted_to_twitter
        else:
            api = twitter.Api(consumer_key=Config.get_config_string('twitter.consumer_key'),
                      consumer_secret=Config.get_config_string('twitter.consumer_secret'),
                      access_token_key=Config.get_config_string('twitter.access_token_key'),
                      access_token_secret=Config.get_config_string('twitter.access_token_secret'))
            tweet = self.compose_tweet()
            status = api.PostUpdate(tweet)
            return True

@receiver(pre_save, sender=Photo)
def photo_pre_save(sender, **kwargs):
    instance = kwargs['instance']
    if instance:
        if instance.is_approved:
            instance.is_copied_to_approved_folder = instance.copy_to_approved_folder()
            instance.is_posted_to_twitter = instance.tweet_photo()


class Config(models.Model):
    name = models.CharField(max_length=256, unique=True)
    value = models.CharField(max_length=1024, null=True)

    def __unicode__(self):
        return '%s' %(str(self.name))

    @staticmethod
    def get_config_int(config_name, default_value=0):
        config, config_created = Config.objects.get_or_create(name=config_name, defaults={'value': default_value})
        return int(config.value)

    @staticmethod
    def get_config_string(config_name, default_value=''):
        config, config_created = Config.objects.get_or_create(name=config_name, defaults={'value': default_value})
        return str(config.value)
