from django.conf.urls import patterns, url
from django.conf import settings
from django.conf.urls.static import static

urlpatterns = patterns('',
                       url(r'^$', 'uploader.views.call_for_submissions', name='call_for_submissions'),
                       url(r'contact', 'uploader.views.contact', name='contact'),
                       url(r'photo/(\d+)/$', 'uploader.views.photo_show', name='photo'),
) + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
