from django.contrib import messages
import os


PROJECT_ROOT_FOLDER = '/Users/jmathews/src/timepics'

# SECURITY WARNING: don't run with debug turned on in production!
DEBUG = True

MESSAGE_LEVEL = messages.DEBUG

# SECURITY WARNING: keep the secret key used in production secret!
SECRET_KEY = '+&tgt1i966q$ri%7um12f9!ng^u73_fdfm5w$joq46(v4#mm=w'


# Database
# https://docs.djangoproject.com/en/1.7/ref/settings/#databases

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.sqlite3',
        'NAME': os.path.join(PROJECT_ROOT_FOLDER, 'db.sqlite3'),
    }
}


ALLOWED_HOSTS = ['localhost']

MEDIA_ROOT = os.path.join(PROJECT_ROOT_FOLDER, 'media')
MEDIA_URL = '/media/'

EMAIL_HOST = 'smtp.gmail.com'
EMAIL_PORT = '587'
EMAIL_USE_TLS = True
EMAIL_HOST_USER = 'user@example.org'
EMAIL_HOST_PASSWORD = 'password'
DEFAULT_FROM_EMAIL = 'photoclock@example.org'

ADMINS = ()
MANAGERS = ()
CONTACT_FORM_RECIPIENT = 'photoclock@example.org'