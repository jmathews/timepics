import os
PATH = os.path.abspath(os.path.dirname(__file__))

def relative(path):
    return os.path.abspath(os.path.join(PATH, path))

BUCKET_NAME = "photoclock"

DEBUG = False

# If you set this to False, Django will make some optimizations so as not
# to load the internationalization machinery.
USE_I18N = False

# If you set this to False, Django will not format dates, numbers and
# calendars according to the current locale
USE_L10N = False

MEDIA_ROOT = relative('media')
MEDIA_URL = "https://{0}.s3.amazonaws.com/".format(BUCKET_NAME)

# STATIC_ROOT = relative('static')

STATICFILES_DIRS = (
    relative('static'),
)

STATIC_ROOT = ''

STATIC_URL = "https://{0}.s3.amazonaws.com/".format(BUCKET_NAME)

DEVELOPMENT = False

DEFAULT_FILE_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
STATICFILES_STORAGE = 'storages.backends.s3boto.S3BotoStorage'
AWS_QUERYSTRING_AUTH = False # Don't include auth in every url
AWS_STORAGE_BUCKET_NAME = 'photoclock'

EMAIL_BACKEND = 'django_ses.SESBackend'
SERVER_EMAIL = 'no-reply@photoclock.ca'

#django-contact-form
DEFAULT_FROM_EMAIL = 'contactform@photoclock.ca'
CONTACT_FORM_RECIPIENT = 'photoclock@justinmathews.ca'

MANAGERS = (
    ('Web Manager','manager@photoclock.ca'),
)

try:
    from secure_settings import *
except ImportError:
    pass